<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'expanse');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'EM]B+pZ:!%}| 04.tmI~=&j;`e(%g(R6u5=hW7j@WkDF{. eWxBnjM_syqw80Qy_');
define('SECURE_AUTH_KEY',  'KmU9`ztV7C8D&~gW-8Ae}>c}2B8X[xjf.!,Pt5UF+_x*Z2)g_R4@3sux9ks?@2p>');
define('LOGGED_IN_KEY',    'mLj*mr*{3<)&pmFK4Su|sCYKGhkI0COoulPndAzKi|?YzFQy[PwwWvF%M%jZfEx0');
define('NONCE_KEY',        ')hEJ>x Pt2To^aZ?rK?515UEi,V)8Rg7CeHZ4}8;RF>?GE/hISeJ+GGhL9,pnJ]C');
define('AUTH_SALT',        'Qj:Evt$=]N6HWCIW]aF:=Cx5Hp!i1AfhbSls_uM*(,pxo&+71C0{4bVwup=x>q}z');
define('SECURE_AUTH_SALT', 'GB.M<E5AFpP]J4zl7DmMQ._B;;W]M)]ukLwS6&gzf1CHx2!riAQqG?>l% upGoXi');
define('LOGGED_IN_SALT',   '@35X:)l0E@`P2-!/u^-y4n=W5}tDqFDg;}|CW#[D!cQxX02!R<J3sQ$hy$_m`b*7');
define('NONCE_SALT',       'NB^v#R+vL3FVf@N4k]8LI9j3wV$98Ntnj*@eDlk{/+6J/y;EF}8$8Y^2&pT|msez');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
