<div class="row">
    <div class="col-md-12"><a href="?page=list_expanse">Back to List</a></div>
</div>


<div class="row">
    <div class="col-md-12">
        <form action="/action_page.php">
            <div class="form-group">
                <?php
                if (isset($members) && count($members) > 0) {
                    ?><select class="select_members form-control"><?php
                    ?><option value="-1">Select Member</option><?php
                        foreach ($members as $user_id => $full_name) {
                            ?><option value="<?php echo $user_id; ?>"><?php echo $full_name; ?></option><?php
                        }
                        ?></select><?php
                } else {
                    echo "Members not available.";
                }
                ?>
            </div>
            <div class="form-group">
                <span class="button-checkbox">
                    <button type="button" class="btn" data-color="primary">Jan</button>
                    <input type="checkbox" class="hidden" value="1" />
                </span>
                <span class="button-checkbox">
                    <button type="button" class="btn" data-color="primary">Feb</button>
                    <input type="checkbox" class="hidden" value="2" />
                </span>
                <span class="button-checkbox">
                    <button type="button" class="btn" data-color="primary">Mar</button>
                    <input type="checkbox" class="hidden" value="3" />
                </span>
                <span class="button-checkbox">
                    <button type="button" class="btn" data-color="primary">Apr</button>
                    <input type="checkbox" class="hidden" value="4" />
                </span>
                <span class="button-checkbox">
                    <button type="button" class="btn" data-color="primary">May</button>
                    <input type="checkbox" class="hidden" value="5" />
                </span>
                <span class="button-checkbox">
                    <button type="button" class="btn" data-color="primary">Jun</button>
                    <input type="checkbox" class="hidden" value="6" />
                </span>
                <span class="button-checkbox">
                    <button type="button" class="btn" data-color="primary">Jul</button>
                    <input type="checkbox" class="hidden" value="7" />
                </span>
                <span class="button-checkbox">
                    <button type="button" class="btn" data-color="primary">Aug</button>
                    <input type="checkbox" class="hidden" value="8" />
                </span>
                <span class="button-checkbox">
                    <button type="button" class="btn" data-color="primary">Sep</button>
                    <input type="checkbox" class="hidden" value="9" />
                </span>
                <span class="button-checkbox">
                    <button type="button" class="btn" data-color="primary">Oct</button>
                    <input type="checkbox" class="hidden" value="10" />
                </span>
                <span class="button-checkbox">
                    <button type="button" class="btn" data-color="primary">Nov</button>
                    <input type="checkbox" class="hidden" value="11" />
                </span>
                <span class="button-checkbox">
                    <button type="button" class="btn" data-color="primary">Dec</button>
                    <input type="checkbox" class="hidden" value="12" />
                </span>
            </div>
            <div class="form-group">
                <input type="text" placeholder="Amount">
            </div>
            <div class="form-group">
                <input type="submit" name="submit" value="Submit">
            </div>
        </form>
    </div>
</div>
