<?php
/* 
 * Plugin Name: Expanse
 * Description: Manage monthly/daily expanse of registered users.
 * Version: 1.0
 * Author: Harsh
 */
define( 'EXPANSE_VERSION', '1.0' );
define( 'EXPANSE__PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
// Basic configuration file
$_config = EXPANSE__PLUGIN_DIR.'/config.php';
if( file_exists($_config))
	include_once($_config);
else
	die('File not found '.$_config);

//load plugin
$_plugin = EXPANSE__PLUGIN_DIR.'/classes/front.class.php';
if( file_exists($_plugin))
	include_once($_plugin);
else
	die('File not found '.$_plugin);

$expanse_manage = Front_View::get_instance();
Front_View::init();

//register_activation_hook( __FILE__, array('Front_View', 'activate_plugin'));
//register_deactivation_hook( __FILE__, array('Front_View', 'deactivate_plugin'));