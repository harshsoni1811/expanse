<?php

if (!class_exists('Expanse_Framework')) {
    $_framework = dirname(__FILE__) . DIRECTORY_SEPARATOR . 'expanse-framework.php';
    if (file_exists($_framework))
        include_once ($_framework);
    else
        die('File not found ' . $_framework);
}

class Front_View extends Expanse_Framework {

    private static $ins = null;

    public static function init() {
        add_action('plugins_loaded', array(self::get_instance(), '_setup'));
    }

    public static function get_instance() {
        // create a new object if it doesn't exist.
        is_null(self::$ins) && self::$ins = new self;
        return self::$ins;
    }

    function _setup() {
        $this->expanse_plugin_meta = expanse_get_plugin_meta();
        add_shortcode($this->expanse_plugin_meta['shortcode'], array(
            $this,
            'render_shortcode_template'
        ));

        add_action('init', array(
            $this,
            'load_custom_post'
        ));

        add_action('wp_enqueue_scripts', array(
            $this,
            'load_scripts'
        ));
    }

    function load_custom_post() {
        register_post_type('user-expanse', array(
            'labels' => array(
                'name' => __('Expanse', 'expanse'),
                'singular_name' => __('Expanse', 'expanse'),
                'add_new' => __('Add New', 'expanse'),
                'add_new_item' => __('Add Expanse', 'expanse'),
                'edit' => __('Edit', 'expanse'),
                'edit_item' => __('Edit Expanse', 'expanse'),
                'new_item' => __('New Expanse', 'expanse'),
                'view' => __('View', 'expanse'),
                'view_item' => __('View Expanse', 'expanse'),
                'search_items' => __('Search Expanse', 'expanse'),
                'not_found' => __('No Expanse found', 'expanse'),
                'not_found_in_trash' => __('No Expanse found in Trash', 'expanse'),
                'parent' => __('Parent Expanse', 'expanse')
            ),
            'public' => true,
            'supports' => array(
                'title',
                'editor',
                'custom-fields'
            ),
//				'menu_icon' => $this->plugin_meta ['logo'] 
        ));
    }

    function render_shortcode_template() {
        ob_start();
        if (isset($_GET['page']) && $_GET['page'] == "add_expanse") {
            $members = array();
            $args1 = array(
                'role' => 'subscriber',
                'orderby' => 'user_nicename',
                'order' => 'ASC'
            );
            $subscribers = get_users($args1);
            if (isset($subscribers) && count($subscribers) > 0) {
                foreach ($subscribers as $user) {
                    $members[$user->ID] = $user->display_name;
                }
            }

            $data = array('members' => $members);
            $this->load_template('template_add.php', $data);
        } else if (!isset($_GET['page']) || $_GET['page'] == "list_expanse") {
            $this->load_template('template_list.php');
        }
        $output_string = ob_get_contents();
        ob_end_clean();
        return $output_string;
    }

    function load_template($file_name, $data = array()) {
        extract($data);
        $file_path = $this->expanse_plugin_meta['path'] . DIRECTORY_SEPARATOR . 'templates' . DIRECTORY_SEPARATOR . $file_name;
        if (file_exists($file_path))
            include_once($file_path);
        else
            die('Could not load file ' . $file_path);
    }

    function load_scripts() {
        wp_enqueue_style('bootstrap', $this->expanse_plugin_meta['url'] . "/inc/css/bootstrap.min.css");
        wp_enqueue_style('select2_min_css', $this->expanse_plugin_meta['url'] . "/inc/css/select2.min.css");
        wp_enqueue_script('select2_js', $this->expanse_plugin_meta['url'] . "/inc/js/select2.js", array('jquery'), null, true);
        wp_enqueue_script('bootstrap_min_js', $this->expanse_plugin_meta['url'] . "/inc/js/bootstrap.min.js", array('jquery'), null, true);
        wp_enqueue_script('select2_custom_js', $this->expanse_plugin_meta['url'] . "/inc/js/custom.js", array('jquery'), null, true);
    }

}
