<?php
$expanse_plugin_dir = 'expanse';
$expanse_plugin_meta = array('shortcode'=>'expanse-cal',
                             'url'=> plugins_url( $expanse_plugin_dir , dirname(__FILE__) ),
                             'path' => WP_PLUGIN_DIR . DIRECTORY_SEPARATOR . $expanse_plugin_dir);

function expanse_get_plugin_meta(){
	global $expanse_plugin_meta;
	return $expanse_plugin_meta;
}
